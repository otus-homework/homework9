﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework9
{
    public static class Extensions
    {
        public static T GetMax<T>(this IEnumerable<T> self, Func<T, float> func) where T : class
        {
            T max = self.FirstOrDefault();
            foreach (var item in self)
            {
                if (func(item) > func(max))
                {
                    max = item;
                }
            }
            return max;
        }
    }
}
