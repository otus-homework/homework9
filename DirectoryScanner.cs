﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Homework9
{
    public class DirectoryScanner
    {

        private readonly string _inputPath;
        private List<FileInfo> _fileInfoList;
        protected internal event EventHandler<FileArgs> _fileFound;
        CancellationTokenSource _token;
        public DirectoryScanner(string inputPath)
        {
            _inputPath = inputPath;
            _fileFound += Show;
        }
        public int FileCounted { get {return _fileInfoList.Count; } }
        public int DirectoryFileCount { get; set; }

        public string MaxSizeFileName { get { return _fileInfoList.GetMax(x=>x.Length).Name; } }
        public void Scan()
        {
            Console.WriteLine("Нажмите Ctrl+С для остановки поиска");
            Console.CancelKeyPress += Console_CancelKeyPress;

            _token = new CancellationTokenSource();
            _fileInfoList = new List<FileInfo>();

            if (Directory.Exists(_inputPath))
            {
                var files = Directory.GetFiles(_inputPath);
                DirectoryFileCount = files.Length;
                foreach (var fileName in files)
                {
                    _fileFound?.Invoke(this, new FileArgs(fileName));
                    Thread.Sleep(1000);
                    _fileInfoList.Add(new FileInfo(fileName));
                    if (_token.IsCancellationRequested)
                        break;
                }
            }
        }

        private void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            if (e.SpecialKey == ConsoleSpecialKey.ControlC)
            {
                e.Cancel = true;
                _fileFound -= Show;
                _token.Cancel();
            };
        }

        public static void Show(object sender, FileArgs e)
        {
            Console.WriteLine($"Файл: {e.FileName}");
        }
    }
}
