﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework9
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите каталога для поиска: ");
            string inputPath = Console.ReadLine();

            //DirectoryScanner directoryScanner = new DirectoryScanner(@"C:\");
            DirectoryScanner directoryScanner = new DirectoryScanner(inputPath);
            directoryScanner.Scan();

            Console.WriteLine($"Всего просканировано файлов: {directoryScanner.FileCounted} из {directoryScanner.DirectoryFileCount}");
            Console.WriteLine($"Файл с наибольшим размером: {directoryScanner.MaxSizeFileName}");

            Console.ReadKey();
        }
    }
}
